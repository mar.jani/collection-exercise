package pl.mj;

import com.sun.deploy.net.MessageHeader;

import java.util.*;

public class Main {
    // private List<String> exampleList;
    // private Set<String> exampleSet;
    // private Map<Integer, String> exampleMap;
    // private Queue<String> exampleQueue;

    private static List<Elf> elfs = new ArrayList<>();
    private static Set<Orc> orcs = new HashSet<>();
    private static Set<Dwarf> dwarfs = new TreeSet<>();

    public static void main(String[] args) {

        addElf();
        showElfs();

        try {
            elfs.remove(1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        showElfs();

        System.out.println(elfs.size());
        System.out.println(elfs.isEmpty());
        System.out.println(elfs.get(0));

//        System.out.println(exampleList.contains("one"));
//        System.out.println(exampleList.indexOf("two"));

        addOrc();
        showOrcs();

    }

    private static void addElf() {
        Elf elf = new Elf();
        elf.setName("Legolas");
        elf.setAge(25);

        elfs.add(elf);
    }

    private static void showElfs() {
        System.out.println("----------Elfs (List - Arraylist)-----------");
        for (Elf elf : elfs) {
            System.out.println(elf.getName() + ", " + elf.getAge());
        }
    }

        private static void addOrc(){
            Orc orc = new Orc();
            orc.setName("One");
            orc.setPower(100);

            orcs.add(orc);
        }

        private static void showOrcs () {
            System.out.println("----------Orcs (Set - Hashset)-----------");
            for (Orc orc : orcs) {
                System.out.println(orc.getName() + ", " + orc.getPower());
            }
        }



}
